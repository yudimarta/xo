function xo(str) {
  let newString = str.toLowerCase()
  let newArray = []
  let newArray2 = []
  for(let i = 0; i <= newString.length; i++){
    if (newString[i] == 'x'){
      newArray.push(newString[i])
    } else if(newString[i] == 'o'){
      newArray2.push(newString[i])
    } else {
      continue
    }   
  }

  if (newArray.length == newArray2.length){
    return true
  } else {
    return false
  }
}

// TEST CASES
console.log(xo('xoxoxo')); // true
console.log(xo('oxooxo')); // false
console.log(xo('oxo')); // false
console.log(xo('xxxooo')); // true
console.log(xo('xoxooxxo')); // true